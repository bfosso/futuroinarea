![](main_data_dir/logo.jpg)  
*Futuro in Area* è un insieme di giornate organizzate grazie all'appassionato lavoro di strutturati e non che lavorano 
presso la nostra Area della Ricerca del CNR di Bari.  
Questi eventi sono pensati per laureandi, phd, postdoc e ricercatori under 40 che vogliono mettersi in gioco facendo 
conoscere le proprie attività di ricerca e sono desiderosi di espandere la propria rete di collaborazioni.
---
# **Gruppo di Coordinamento**
Il Gruppo di coordinamento è attualmente formato da 6 persone che si occupano dell'organizzazione degli eventi.

**Nome**|**Cognome**|**Istituto**|**Mail**|
--------|-----------|------------|---------|
Danilo|Belviso|IC|[danilo.belviso@ic.cnr.it](mailto:danilo.belviso@ic.cnr.it)|
Giuseppe|Mangiatordi|IC|[giuseppe.mangiatordi@ic.cnr.it](mailto:giuseppe.mangiatordi@ic.cnr.it)|
Martina|Loi|ISPA|[martina.loi@ispa.cnr.it](mailto:martina.loi@ispa.cnr.it)|
Angela|Martiradonna|IAC|[a.martiradonna@ba.iac.cnr.it](mailto:a.martiradonna@ba.iac.cnr.it)|
Livia|Zuffianò|IRPI|[l.zuffiano@ba.irpi.cnr.it](mailto:l.zuffiano@ba.irpi.cnr.it)|
Bruno|Fosso|IBIOM|[b.fosso@ibiom.cnr.it](mailto:b.fosso@ibiom.cnr.it)|

### Per qualsiasi comunicazione circa gli eventi di **_Futuro in Area_** potete scrivere a [futuroinarea@area.ba.cnr.it](mailto:futuroinarea@area.ba.cnr.it).
---
# **Referenti di Istituto**
I referenti di Istituto sono le persone che fungono da punto di conttato tra il gruppo che coordina gli eventi ed i 
ricercatori interessati a condividere i risultati del proprio lavoro. 

**Nome**|**Cognome**|**Istituto**|**Mail**|
--------|-----------|------------|---------|
Antonio|Petitti|STIIMA|[antonio.petitti@stiima.cnr.it](mailto:antonio.petitti@stiima.cnr.it)|
Bachir|Balech|IBIOM|[b.balech@ibiom.cnr.it](mailto:b.balech@ibiom.cnr.it)|
Olga|de Pascale|NANOTEC|[olga.depascale@cnr.it](mailto:olga.depascale@cnr.it)
Alessandra|Villani|ISPA|[alessandra.villani@ispa.cnr.it](mailto:alessandra.villani@ispa.cnr.it)
Angela|Martiradonna|IAC|[a.martiradonna@ba.iac.cnr.it](mailto:a.martiradonna@ba.iac.cnr.it)|
Mariella|Aquilino|IIA|[aquilino@iia.cnr.it](mailto:aquilino@iia.cnr.it)
Arianna|Consiglio|ITB|[arianna.consiglio@ba.itb.cnr.it](mailto:mailto:arianna.consiglio@ba.itb.cnr.it)
Nicola|Corriero|IC|[nicola.corriero@ic.cnr.it](mailto:nicola.corriero@ic.cnr.it)
---
# Programma
- [X] **15 Maggio 2019**   
    - [X] ore 10.00  [**Nicola Corriero (IC)**](https://scholar.google.it/citations?hl=it&user=beoImLYAAAAJ)   
        ["**Metodi avanzati e software per la caratterizzazione di composti policristallini**"](https://drive.google.com/open?id=1f8xSenPNrtQV37H54MucBANWmOTzMeLo)    
    - [X] ore 10.45   **Andrea Montanaro (IAC)**  
        "**Gli Iapigi, gli Etruschi e gli altri popoli. Aspetti sconosciuti della Puglia in età preromana.**" 
    - [X] ore 11.30 [**Bruno Fosso (IBIOM)**](https://scholar.google.it/citations?hl=it&user=TBeT9pIAAAAJ)   
        ["**Lo studio del microbioma e le sue molteplici applicazioni**"](https://drive.google.com/open?id=1hQg79Ij-e3SGxmMgnJSFhlV0j6JvyKU4) 
    
- [ ] **17 Luglio 2019** 
    - [X] ore 10.00  [**Marco Tangaro (IBIOM)**](https://www.researchgate.net/profile/Marco_Tangaro)  
        ["**Workflow in ambiente cloud**"](https://drive.google.com/open?id=13OIB6868LLwPxeUuuo6YP9qJwB2CiN3iXnDGd_lGsMk)  
    - [X] ore 10.45 **Massimo Dell’edera (IPCF)**  
        "**Nano-TiO2 modified surface based for wastewater treatment application**" 
    - [X] ore 11.30 [**Angela Martinadonna (IAC)**](https://scholar.google.it/citations?hl=it&user=41_o3hcAAAAJ)  
        ["**Modellistica differenziale per il controllo delle specie invasive**"](https://drive.google.com/open?id=1VexgMJjY0O2WRaxkfA3IkQ3ukHJlkFhtNk1u1rqy07U) 
    
- [ ] **23 Ottobre 2019** 
    - [ ] ore 10.00 **Federica Rizzi (IPCF)**   
    "**Nanoformulations based on multifunctional colloidal inorganic nanoparticles for theranostic applications**"   
    - [ ] ore 10.45 **Pietro del Re (IC)**  
    "**Comprendere i meccanismi fisiologici attraverso la dinamica molecolare**" 
    - [ ] ore 11.30 **Fabio Vischio (IPCF)**   
    "**Plasmonic nanosystems for cancer treatment**" 
    
- [ ] **21 Aprile 2020** 
    - [ ] ore 10.00 [**Giuseppe Mangiatordi (IC)**](https://scholar.google.it/citations?hl=it&user=02ZPB9oAAAAJ)  
    "**Il regolamento REACH e la tossicologia predittiva**" 
    - [ ] ore 10.45 **Alberta Terzi (IC)**  
    TBD 
    - [ ] ore 11.30 [**Flaviana Marzano (IBIOM)**](https://scholar.google.it/citations?hl=it&user=l4LvfHMAAAAJ)  
    TBD
  
- [ ] **1 Luglio 2020** 
    - [ ] ore 10.00  [**Benny Danilo Belviso (IC)**](https://scholar.google.it/citations?hl=it&user=jALaKoUAAAAJ)   
    "**Exploring protein-ligand interactions by X-ray based techniques**" 
    - [ ] ore 10.45  [**Marinella Marzano (IBIOM)**](https://scholar.google.it/citations?hl=it&user=LKB2nuMAAAAJ)  
    TBD
    - [ ] ore 11.30  [**Mariangela De Robertis (IBIOM)**](https://scholar.google.it/citations?hl=it&user=T-wt57kAAAAJ)  
    TBD 